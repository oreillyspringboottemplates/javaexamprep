package com.oreillyauto.problems.carInheritance;
abstract class Car {
    protected boolean isSedan;
    protected String seats;
    
    public Car(boolean isSedan, String seats) {
        this.isSedan = isSedan;
        this.seats = seats;
    }
    
    public boolean getIsSedan() {
        return this.isSedan;
    }
    
    public String getSeats() {
        return this.seats;
    }
    
    abstract public String getMileage();
    
    public String printCar(String name) {
        String response = "A " + name + " is " + (this.getIsSedan() ? "" : "not ") 
                + "Sedan, is " + this.getSeats() + "-seater, and has a mileage of around "
                + this.getMileage() + "."; 

        return response;
    }
}