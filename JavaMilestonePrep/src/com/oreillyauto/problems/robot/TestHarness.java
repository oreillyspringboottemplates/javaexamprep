package com.oreillyauto.problems.robot;
            
/**
 * The Robot Class
 * 
 * Create a Robot class that can move around on a two dimensional plane. 
 * It needs to be able to change its position, report its position and report 
 * its last move as described below. Implement a Robot class per the following 
 * specifications:
 * 
 * Fields
 * Data Type Name      Description
 * integer   currentX  The robot's current x-coordinate in the 2D plane.
 * integer   currentY  The robot's current y-coordinate in the 2D plane.
 * integer   previousX The robot's x-coordinate in the 2D plane prior to its most recent movement.
 * integer   previousY The robot's y-coordinate in the 2D plane prior to its most recent movement.
 * 
 * Note: The robot's initial location is at (x, y) coordinate (0, 5). 
 *  
 * Parameterized Constructor:
 * Data Type   ParamName  Description
 * integer     x          The value of currentX for the new Robot.
 * integer     y          The value of currentY for the new Robot.
 * 
 * Note: The robot created by this constructor is considered to have spawned at (0, 5) 
 * and moved to (currentX, currentY) so (previousX, previousY) starts as (0, 5).
 * 
 * --METHOD 1--
 * Return:      void
 * Method:      moveX(Integer dx)
 * Description: Move the robot from current position (x, y) to new position 
 *              (x + dx, y). Remember to maintain previousX.
 *              
 * --METHOD 2--
 * Return:      void
 * Method:      moveY(Integer dy)
 * Description: Move the robot from current position (x, y) to new position 
 *              (x, y + dy). Remember to maintain previousY.
 *                            
 * --METHOD 3--
 * Return:      String
 * Method:      printCurrentCoordinates();
 * Description: Return two space-separated integers describing the robot's 
 *              current x and y coordinates.
 * 
 * --METHOD 4--
 * Return:      String
 * Method:      printLastCoordinates()
 * Description: Return two space-separated integers describing the robot's 
 *              previousX and previousY coordinates. This will be called after 
 *              the robot has moved from position (0, 5) at least once.
 * 
 * --METHOD 5--
 * Return:      String
 * Method:      printLastMove()
 * Description: Return two space-separated values describing the robot's most recent movement:
 *              - If the last move was moveX(dx), print x dx where x is the actual character x 
 *                and dx is the distance moved in the x-direction during the last call to moveX.
 *              - If the last move was moveY(dy), print y dy where y is the actual character y 
 *                and dy is the distance moved in the y-direction during the last call to moveY.
 *  
 * The code provided has a complete definition for a main method that creates Robot objects and 
 * tests the class methods. Implement the Robot class according to the criteria above to pass 
 * all test cases.
 *  
 * - Create the Robot class and methods
 * - DO NOT use an access modifier for your classes!
 * - Uncomment the code below
 * - Execute TestHarness.java to see the results. 
 *  
 *  HINT HINT HINT HINT!!!!!!!
 *  If you update one thing, always update the other thing; even if you don't think you need to.
 *  
 * @author jbrannon5
 *
 */
public class TestHarness {
    /**
     *   DO NOT MODIFY THIS CLASS!!
     */
    
    private static final String EXPECTED = 
            "0 5\n" + 
            "2 1\n" + 
            "x 1\n" + 
            "3 1\n" + 
            "3 1\n" + 
            "x 2\n" + 
            "5 2\n" + 
            "5 2\n"; 
    /**
     * Input:
     *   x=2, y=, dx=1, dy=1
     *  
     * Expected Output:
     * 0 5
     * 2 1
     * x 1
     * 3 1
     * 3 1
     * x 2
     * 5 2
     * 5 2
     * 
     * @param args
     */
    public static void main(String[] args) {
        int x = 2;
        int y = 1;
        int dx = 1;
        int dy = 1;
        StringBuilder sb = new StringBuilder();
        
//        Robot firstRobot = new Robot();
//        sb.append(firstRobot.printCurrentCoordinates() + "\n");
//        
//        Robot secondRobot = new Robot(x, y);
//        sb.append(secondRobot.printCurrentCoordinates() + "\n");
//        
//        for (int i = 1; i < 3; i++) {
//            secondRobot.moveX(dx);
//            sb.append(secondRobot.printLastMove() + "\n");
//            sb.append(secondRobot.printCurrentCoordinates() + "\n");
//            secondRobot.moveY(dy);
//            sb.append(secondRobot.printLastCoordinates() + "\n");
//
//            dx += i * i;
//            dy -= i * i;
//        }
//        
//        System.out.println("OUTPUT:\n" + sb.toString());
//        System.out.println("EXPECTED:\n" + EXPECTED);
        
        if (EXPECTED.equalsIgnoreCase(sb.toString())) {
            System.out.println("1/1 Scenarios Passed");
        } else {
            System.out.println("0/1 Scenarios Passed");
        }
        
    }
}
