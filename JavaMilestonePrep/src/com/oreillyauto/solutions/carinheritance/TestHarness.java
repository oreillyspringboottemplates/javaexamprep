package com.oreillyauto.solutions.carinheritance;

import java.io.IOException;

/**
 *  Build on an abstract class (Car.java) and initialize an instance of each class 
 *  with a variable. The program will then test the implementation by retrieving 
 *  the stored data.
 *  
 *  The main method does the following (DO NOT MODIFY THE MAIN METHOD!) :
 *  - Declares an abstract class named Car with the implementations for getIsSedan() 
 *    and getSeats() methods and an abstract method named getMileage().
 *  
 *  - Creates WagonR, HondaCity, or InnovaCrysta object based on input (0 for WagonR, 
 *    1 for HondaCity and 2 for InnovaCrysta).
 *    
 *  - Calls the getIsSedan(), getSeats(), and getMileage() methods on the object.
 *  
 *  The details for each car are provided below:
 *    WagonR is not a sedan and has 4 seats.
 *    HondaCity is a sedan and has 4 seats.
 *    InnovaCrysta is not a sedan and has 6 seats.
 *   
 *  Function Description:
 *   
 *    Build classes and implement the following:
 *      - Create classes named WagonR, HondaCity, and InnovaCrysta that all inherit from 
 *        the Carclass.
 *      - Each class must have a constructor that receives one integer argument representing 
 *        the mileage of the car.
 *      - Each class must implement a getMileage() method which returns a string in the form 
 *        of '<mileage> kmpl' where <mileage> is the value provided to the constructor.
 *   
 *  Constraints:
 *    0 <= type of car <= 2
 *    5 <= mileage <= 30
 *  
 *  - Create the necessary classes to complete this challenge.
 *  - DO NOT use an access modifier for your classes!  
 *  - Uncomment the main method.
 *  - Execute the TestHarness to see the results.
 *  
 * @author jbrannon5
 *
 */
public class TestHarness {
    private final static String SCENARIO_ONE = "A WagonR is not Sedan, is 4-seater, and has a mileage of around 15 kmpl.";
    private final static String SCENARIO_TWO = "A HondaCity is Sedan, is 4-seater, and has a mileage of around 15 kmpl.";
    private final static String SCENARIO_THREE = "A InnovaCrysta is not Sedan, is 6-seater, and has a mileage of around 30 kmpl.";
    private static int carMileage = 0;
    private static int carType = 0;
    private static int scenariosPassed = 0;
    private static int totalScenarios = 0;
    
    public TestHarness() {
    
    }

    public static void main(String[] args) throws IOException {
        String userOutput = "";
        
        for (int i=0; i< 3; i++) {
            carType = i;
            totalScenarios+=1;
            carMileage = ((i==0) ? 15 : (i*15));
            
            if (carType == 0){
                Car wagonR = new WagonR(carMileage);
                wagonR.printCar("WagonR");
            }
            
            if(carType == 1){
                Car hondaCity = new HondaCity(carMileage);
                hondaCity.printCar("HondaCity");
            }
            
            if(carType == 2){
                Car innovaCrysta = new InnovaCrysta(carMileage);
                innovaCrysta.printCar("InnovaCrysta");
            }
            
            switch(carType) {
                case 0: 
                    userOutput = new WagonR(carMileage).printCar("WagonR");
                    reportScenario(0, SCENARIO_ONE, userOutput);
                    break;
                case 1: 
                    userOutput = new HondaCity(carMileage).printCar("HondaCity");
                    reportScenario(1, SCENARIO_TWO, userOutput);
                    break;
                case 2:
                    userOutput = new InnovaCrysta(carMileage).printCar("InnovaCrysta");
                    reportScenario(2, SCENARIO_THREE, userOutput);
                    break;
            }
        }
        
        System.out.println("\n" + scenariosPassed + "/" + totalScenarios+ " Scenarios Passed");
    }

    private static void reportScenario(int number, String scenario, String userOutput) {
        if (scenario.equalsIgnoreCase(userOutput)) {
            System.out.println("\n+--- SCENARIO "+number+" - (PASS) ---+");
            scenariosPassed+=1;
        } else {
            System.out.println("\n+--- SCENARIO "+number+" - (FAIL) ---+");
        }
        
        System.out.println("Output:   " + userOutput);
        System.out.println("Expected: " + scenario);
    }
    
}
