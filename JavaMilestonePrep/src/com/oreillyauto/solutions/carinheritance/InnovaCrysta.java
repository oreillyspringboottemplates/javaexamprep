package com.oreillyauto.solutions.carinheritance;
class InnovaCrysta extends Car {
    int mileage;
    public final static boolean IS_SEDAN = false;
    public final static String SEAT_COUNT = "6";
    
    public InnovaCrysta(int mileage) {
        super(IS_SEDAN, SEAT_COUNT);
        this.mileage = mileage;
    }
    
    public String getMileage() {
        return mileage + " kmpl" ;
    }
}