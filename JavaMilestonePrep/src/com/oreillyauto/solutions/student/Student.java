package com.oreillyauto.solutions.student;

class Student {
    static int counter = 0;
    private String name;
    private int enrollmentNumber; 
    
    public Student(String name) {
        this.name = name;
        this.enrollmentNumber = ++counter;
    }

    @Override
    public String toString() {
        return enrollmentNumber + ": " + name;
    }
}
