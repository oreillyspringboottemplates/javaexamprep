package com.oreillyauto.solutions.compusa;

public class TestHarness {

    private static final String EXPECTED = "OUT_OF_STOCK: Out of stock.\n" + 
            "Laptops successfully added.\n" + 
            "INVALID_COUNT: Count should be greater than zero.\n" + 
            "Laptops successfully sold.\n" + 
            "SERIAL_NUMBER_MISSING: Serial Number Missing.\n" + 
            "INVALID_COUNT: Count should be greater than zero.\n" + 
            "Laptops successfully added.\n" + 
            "INVALID_COUNT: Count should be greater than zero.\n" + 
            "Laptops successfully sold.\n" + 
            "Laptops successfully added.\n";
    
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        Laptop laptop = new Laptop("Dell", "m6800");
        Laptop laptop2 = new Laptop("HP", null);

        try {
            sb.append(LaptopTransaction.sellLaptop(laptop, 50) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop, 100) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop, 0) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.sellLaptop(laptop, 30) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop2, 500) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop, -5) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop, 1000) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.sellLaptop(laptop, -20) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        try {
            sb.append(LaptopTransaction.sellLaptop(laptop, 100) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        try {
            sb.append(LaptopTransaction.addLaptop(laptop, 720) + "\n");
        } catch (TransactionException e) {
            sb.append(e.getErrorCode() + ": " + e.getErrorMessage() + "\n");
        }
        
        System.out.println("OUTPUT:\n" + sb.toString());
        System.out.println("EXPECTED:\n" + EXPECTED);
        
        if (EXPECTED.equalsIgnoreCase(sb.toString())) {
            System.out.println("1/1 Scenarios Passed");
        } else {
            System.out.println("0/1 Scenarios Passed");
        }
    }

}
