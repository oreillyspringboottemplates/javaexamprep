package com.oreillyauto.solutions.compusa;

public class TransactionException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private String errorCode;
	
	public TransactionException(String errorMessage, String errorCode) {
		this.errorCode=errorCode;
		this.errorMessage= errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
}
