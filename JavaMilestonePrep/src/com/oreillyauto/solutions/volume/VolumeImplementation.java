package com.oreillyauto.solutions.volume;

public class VolumeImplementation extends Volume {
    int gallons = 0;
    float ounces = 0;
    int factor = 128;
    
    public VolumeImplementation() {
    }
    
    @Override
    void setGallonsAndOunces(int gallons, float ounces) {
        this.gallons = gallons;
        this.ounces = ounces;
    }
    
    @Override
    int getGallons() {
        return gallons;
    }
    
    @Override
    public float getOunces() {
        return ounces;
    }
    
    @Override
    String getVolumeComparison(Volume volume) {
        float thisDist = this.gallons * factor + this.ounces;
        float thatDist = volume.getGallons() * factor + volume.getOunces();
        
        if (thisDist > thatDist) {
            return ("First volume is larger.");
        } else if (thisDist < thatDist) {
            return ("Second volume is greater.");
        } else {
            return ("Both volumes are equal.");
        }
    }

}
