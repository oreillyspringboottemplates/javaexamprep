package com.oreillyauto.solutions.cuisine;

class FoodFactory {
    static FoodFactory factory; // Singleton instance of this class
    java.util.Map<String, Cuisine> cmap = new java.util.HashMap<String, Cuisine>();
    
    public static FoodFactory getFactory() {
        if (factory == null) {
            factory = new FoodFactory();
        }
        
        return factory;
    }
    
    void registerCuisine(String cuisineKey, Cuisine cuisine) {
        cmap.put(cuisineKey, cuisine);
    }
    
    Cuisine serveCuisine(String cuisineKey, String dish) throws UnservableCuisineRequestException {
        if (cmap.containsKey(cuisineKey) == false) {
            throw new UnservableCuisineRequestException("Unservable cuisine " + cuisineKey + " for dish " + dish);
        }
        
        return cmap.get(cuisineKey).serveFood(dish);
    }  
}