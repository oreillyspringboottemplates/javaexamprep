package com.oreillyauto.solutions.geometry;

public interface Square {
    public int area(int length);
}