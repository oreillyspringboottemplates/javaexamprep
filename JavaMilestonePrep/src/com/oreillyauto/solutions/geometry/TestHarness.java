package com.oreillyauto.solutions.geometry;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Build a Geometry class that implements the interfaces:
 *  Square
 *  Triangle
 *  
 * Use one half the base times the height to calculate the 
 * area of a triangle. Use length times width to calculate
 * the area of a square.
 * 
 * Uncomment the main method once you have created the 
 * Geometry class.
 * 
 * EXPECTED OUTPUT (what you should see in the console after running TestHarness.java):
 * I implemented: 
 * Square
 * Triangle
 * Triangle area = 6
 * Square area = 36
 * 
 * - Implement Geometry.java
 * - DO NOT use an access modifier for your classes!
 * - Uncomment the code below
 * - Execute TestHarness.java to see the results.
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {
    private static StringBuilder sb = new StringBuilder();
    private static final String EXPECTED = "I implemented: \n" + 
            "Square\n" + 
            "Triangle\n" + 
            "Triangle area = 6\n" + 
            "Square area = 36\n";
    
    public static void main(String[] args) {        
        Geometry g = new Geometry();
        sb.append("I implemented: \n");
        implementedInterfaceNames(g);
        int base = 2;
        int height = 6;
        sb.append("Triangle area = " + g.area(base, height) + "\n");
        sb.append("Square area = " + g.area(height) + "\n");
        
        System.out.println("Output: \n" + sb.toString());
        System.out.println("Expected: \n" + EXPECTED);
        
        if (EXPECTED.equalsIgnoreCase(sb.toString())) {
            System.out.println("1/1 Scenarios Passed");
        } else {
            System.out.println("0/1 Scenarios Passed");
        }
    }

    static void implementedInterfaceNames(Object o) {
        Class[] interfaces = o.getClass().getInterfaces();
        
        Arrays.sort(interfaces, new Comparator<Class>() {
            public int compare(Class o1, Class o2) {
                return o1.getSimpleName().compareTo(o2.getSimpleName());
            }
        });
        
        for (Class c : interfaces) {
            sb.append(c.getSimpleName() + "\n");
        }
    }

}
